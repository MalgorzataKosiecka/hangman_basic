import random
import string

words_dictionary = {"colors": "pink red maroon brown salmon coral orange gold ivory yellow olive green aquamarine "
                              "turquoise azure lavender blue navy indigo violet plum magenta purple beige gray "
                              "white silver black".split(),
                    "fruits": "apple apricot avocado banana blackberry blackcurrant cherry coconut cranberry fig "
                              "grape grapefruit lemon lime lychee mango watermelon orange papaya pear plum pineapple "
                              "pomegranate raspberry "
                              "strawberry".split(),
                    "animals": "ant bird cat chicken cow dog elephant fish fox hamster horse kangaroo lion monkey "
                               "penguin pig rabbit sheep tiger whale wolf".split(),
                    "emotions": "fear anger sadness joy disgust surprise trust anticipation".split()}


def secret_word_choice(dictionary):
    secret_word_key = random.choice(list(dictionary.keys()))
    secret_word_value = random.choice(dictionary[secret_word_key])
    secret_word_listed = list(secret_word_value)
    return secret_word_key, secret_word_value, secret_word_listed


def difficulty_level():
    difficulty_level = input("Choose difficulty_level: L for Low, M for Medium, H for High: ").upper()
    if difficulty_level == "L":
        number_of_tries = 9
        print(f"Your choice is 'low' difficulty level. You can make {number_of_tries - 1} errors.")
    elif difficulty_level == "M":
        number_of_tries = 6
        print(f"Your choice is 'medium' difficulty level. You can make {number_of_tries - 1} errors.")
    elif difficulty_level == "H":
        number_of_tries = 3
        print(f"Your choice is 'high' difficulty level. You can make {number_of_tries - 1} errors.")
    else:
        number_of_tries = 1
        print(f"Please comply to rules ;) Now you will play default 'ultra super high' difficulty level. "
              f"You can make {number_of_tries - 1} errors.")
    return number_of_tries


def game_information(number_of_tries, used_letters_list):
    if number_of_tries > 1:
        print(f"You have {number_of_tries} more tries.")
    elif number_of_tries == 1:
        print(f"You have {number_of_tries} more try.")
    print(f"You used letters {used_letters_list}")


def user_letter_check(already_given):
    letter = input("Enter a letter: ")
    letter = letter.lower()
    if len(letter) != 1:
        print("Please type only one letter.")
    elif letter in already_given:
        print("This letter has already been used.")
    elif letter not in string.ascii_lowercase:
        print("Please type a letter.")
    else:
        return letter


def game_course():
    number_of_tries = difficulty_level()

    category, word, listed_word = secret_word_choice(words_dictionary)
    print("\nCategory of the word: " + category.upper() + "\n")

    used_letters_list = []

    for i in range(len(word)):
        listed_word[i] = "_"

    while number_of_tries > 0:
        print(" ".join(listed_word))

        try:
            used_letter = user_letter_check(used_letters_list)
            if used_letter != None:
                used_letters_list.append(used_letter)
            else:
                pass

            if used_letter in word:
                for i in range(len(word)):
                    if word[i] == used_letter:
                        listed_word[i] = used_letter

                if "".join(map(str, listed_word)) == word:
                    print(f"\nYou won!!! You guessed the word '{word}' :)")
                    break

            else:
                number_of_tries -= 1

            print()

            game_information(number_of_tries, used_letters_list)

        except TypeError:
            pass

    else:
        print(f"\nYou didn't guess the word '{word}' :(")


print("\nWELCOME TO HANGMAN GAME\n")


game_course()
